#buildpkg

A python script which automates the sending of options to configure scripts or build systems. Currently the only implemented systems are the `autotools` sytem and the `cmake` system.

Why Use?
--------

This script is an attempt at providing a unified way to build and install source packages using a number of build systems. Currently, the only supported systems are `autotools` and `cmake`. This removes much of the headache of having to re-build and install packages during development and allows the user to quickly build and install an unknown package. The issue is that different build systems often have vastly different methods of specifying build, and install directories, or can include an additional configuration step which varies as well. This provides a script which will look similar for every source package you need to install.

Quick Start
-----------

Once installed, all you have to do is call the script with `buildpkg --name Package` along with an argument defining which build system to use `--autotools` for the `autotools` build system, and `--cmake` for the `cmake` build system and also an argument telling the script what to do. For instance `--all` configures, builds, and installs the source package. This script will now look in the current directory for a directory named `Package` which contains the source files. By default, it will perform an out-of-tree build in a new directory it will create at `Package-build` and install at a new directory it will create `Package-install` both in the current directory. Some build systems aren't capable of performing out-of-tree builds. These will fail and either tell you directly, or throw some kind of error which should lead you to the conclusion that the source package doesn't support out-of-tree building. Alternat build and install directories as well as configuration arguments can be passed as well. Here is a short summary of the arguments and what they do.

`--name` Takes the name of the source package to build. It looks in the current directory for a directory of the same name.

Build systems:

`--autotools` Tells the script to use the `autotools` build system

`--cmake` Tells the script to use the `cmake` build system

Actions:

`--all` Configure, Build, and Install the source package

`--config` Configure the source package

`--build` Build the source package

`--install` Install the source package

`--clean` Clean the build and install directories

Additional helpful options:
`--config-arg` Pass an option which you wish to pass to the configuration step. For instance, many source packages allow for the selective activation or deactivation of certain features usually passed at the configuration step.

`-j` Pass an integer to use that number of threads in a parallel build

`--source-dir` Tells the script to find the source directory at a certain path you give it.

`--build-dir` Tells the script where to build the source package

`--install-dir` Tells the script where to install the source package

`--no-out-of-tree` Tells the script not to do an out-of-tree build. It will not create a build directory, instead building within the source directory

`--dont-create` Don't create directories, fail instead. If a directory which is needed doesn't exist, Just fail.